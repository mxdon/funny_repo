# learn vim in a few seconds

## 两种模式
一种是命令模式，通过 esc 或者 ctrl [ 可以进入，此状态可以使用命令
一种是编辑模式，通过 i a o I A O 均可以进入，此时可以编辑文本

## 快捷键
vim的灵魂就在于快捷键，可以做到没有键盘也可以很方便的编辑文本，以下说明本次常用的快捷键

gd 高亮显示单词
ctrl ] 跳转到函数定义处
ctrl o 跳转回去
ctrl i 与 ctrl o 相对
d 删除
y 复制
p 粘贴
v 可视化选择文本

## 命令
命令模式下有些常用命令需要知道
:noh	取消高亮
:set nu	显示行号
:vsp file	左侧打开新文件
:sp file	上方打开新文件

差不多够用吧
