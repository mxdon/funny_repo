# How to build a Linux without Hardware

## 1.先决条件

1. Linux开发环境
	
	1.1 vim
	
	1.2 git
	
	1.3 gdb-multiple
	
	1.4 make
	
	1.5 ctags

2. 源码准备
    
    2.1 Linux011 created by mxd
    
    2.2 Linux2.6.32 provided by loongson
    
    2.3 mips_os provided by BUAA
    
    2.4 PMON
    
    2.5 Qemu provided by loongson
    
    2.6 gcc-ls232
    
    2.7 gxemul

3. 需要注意的文件

     3.1 boot/start.S

     3.2 tool/*lds

     3.3 drivers/gxconsole/console.c

4. 需要具备的能力

     4.1 反汇编

     4.2 ctags

     4.3 gdb调试

     ......

## Something useful
传了两个文件，分别是vim的配置文件和git的拼写辅助文件，大家可以直接拿去用，比较方便

## 2.lab0
lab0的内容就是教大家熟悉实验环境，主要是git和Linux的使用方面，这一部分我们只记住常用的命令即可，在note仓库下已经上传了简单的使用说明，同样为了了解git的使用，建议大家使用git来查看

所以lab0的内容就不详细介绍，通过其他的lab来熟悉这些工具。

## 3.lab1
lab1的最终目的是为了让Linux在模拟器上成功跑起来，为此需要做点预备知识。

1. 操作系统的启动流程
	其实可以放大一点了解，给电脑电源键按下，到操作系统运行，中间发生了哪些事？
	电源开启，硬件自动运行ROM下的程序，由于ROM是只读存储器，不可用用作内存，所以也就不支持堆栈，此时只支持简单且少量的汇编代码运行，其实在PMON的start.S汇编代码中，第一段代码内容如下：
	
	```C
   .set    noreorder
    .globl  _start
    .globl  start
	 .globl  __main
	_start:
	start:
		.globl  stack
	stack = start - 0x4000      /* Place PMON stack below PMON start in RAM */
	
		/* NOTE!! Not more that 16 instructions here!!! Right now it's FULL! */
		mtc0    zero, COP_0_STATUS_REG
		mtc0    zero, COP_0_CAUSE_REG
		li  t0, SR_BOOT_EXC_VEC /* Exception to Boostrap Location */
		mtc0    t0, COP_0_STATUS_REG
		la  sp, stack
	la  gp, _gp
	
		bal uncached        /* Switch to uncached address space */
	nop
	
		bal locate          /* Get current execute address */
	nop
	
	uncached:
		or  ra, UNCACHED_MEMORY_ADDR
		j   ra
		nop
	```
	大致扫一眼，可以看出其第一件事便是在设置堆栈，由于只读空间是无法作为堆栈的，而且此时是硬件刚刚启动，内存的初始化也没有做，唯一有的就只有MIPS架构自带的内存结构，也就是那些内存映射机制，此外还剩下的就是计算机所带有的硬件，这里首先注意Cache，Cache是可读写的空间（而且Cache也叫做SRAM），所以也就可以作为堆栈，这项技术也就是传说中的锁Cache，使得Cache As Ram使用，形成一段小小的堆栈，上面代码设置的是0x4000大小，也就是16KB大小，刚好是LS232的Cache大小。
	上面还值得注意的是两条bal指令，第一条是获取uncached空间，地址空间是0xa0000000，这里是获得了一个基地址，有了这个基址，之后的程序只要设定相关的大小，就可以确保其在这一指定空间中运行，然后就跳转去了locate程序，节选部分代码如下：
	```c
   locate:                                                                                                                                                                                
    la  s0,start
    subu    s0,ra,s0
    and s0,0xffff0000
    bal CPU_TLBClear
    nop
    li  t0,SR_BOOT_EXC_VEC
    mtc0    t0,COP_0_STATUS_REG
        mtc0    zero,COP_0_CAUSE_REG
 .set noreorder
   
    bal initserial
    nop
    
    PRINTSTR("\r\nPMON2000 MIPS Initializing. Standby...\r\n")
    PRINTSTR("ERRORPC=")
    mfc0    a0, COP_0_ERROR_PC
    bal hexserial
	 nop
......
	
   do_caches:
	 TTYDBG("Init caches...\r\n")
	......
   bootnow:
	 TTYDBG("Copy PMON to execute location...\r\n")
	......
	PRINTSTR("\ncopy text section done.\r\n")
	......
	TTYDBG("Copy PMON to execute location done.\r\n")
	......
	la  v0, initmips
	......
	```

​	上面有许多条打印指令，通过打印信息也可以判断出程序的功能，首先是启动的第一条打印，理所当然的，然后就是几句Copy，也就是把程序拷贝到相应的地址空间去执行，而且可以看出，拷贝完就进入了initmips，这是一个C函数。
​	所以除去上面的具体细节，也就得到了计算机从按下电源，到系统执行的过程：
​	首先在ROM上执行简短的汇编指令，并将Cache锁存，作为RAM使用，将汇编程序拷贝到CAR空间中，继续执行汇编程序，并在此阶段初始化必要的硬件设备，此时应该是RAM，初始化RAM之后，就有了完整可用的内存空间，这个时候再将C程序拷贝至RAM中运行，并跳转到C的入口，以上便是BootLoader中常划分的Stage1
​	然后Stage2时开始执行C程序，这个时候能力很大，要初始化很多硬件设备供之后操作系统使用，然后载入内核和根文件系统，最后经过一定的设置去跳转到内核入口，这边是Stage2的内容。
​	
​	而我们针对龙芯杯的内核设计和移植工作，便是将Stage2和内核联系在一起的工作。

### 内核入口定位

1. 借助可以启动的内核进行定位

2. 借助PMON源码分析，进行定位

### ELF文件简要介绍
1. 如何查看相关信息

2. 如何写链接脚本

3. 如何对ELF文件反汇编

### 添加异常处理函数
0. 异常处理的内容

1. 加在哪

2. 如何在链接脚本中添加并不影响内核入口

### 其他内容

0. 实战printf

1. 大小端转换

2. 调试功能启用

3. Qemu和gxemul仿真过程

以上大概就是lab0的所有内容了。

## 3.lab1-内存管理

1. TLB
	页表一般都很大，存在于内存中，那么存在页表以后，CPU读取指令和数据需要访问两次内存——1.通过页表查询得到物理地址，2.通过物理地址读取指令和数据。
	
	同理，二级页表就是3次访存，这样看起来有些拖沓的，影响效率，所以除了页表，还有页表的缓存需要了解，也即TLB
	
	在虚拟存储器中，地址被划分为虚页号和页偏移，
	
	页表寄存器

![image-20210528194557832](image/image-20210528194557832.png)
