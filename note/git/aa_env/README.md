
[toc]

千里之行，始于足下。

## Getting Started
提前取得往年团赛资料包，有利于理解 CPU 在大赛起何作用。作为初学者，你不必看懂里面的每份文档；咱们有条不紊地，慢慢来。

[N21_团赛资料包](http://61.153.251.58:10653/nscscc/2021/nscscc2021_group_v0.01.7z) 解压密码 nscscc2021

如果在用 **Windows**：
1. [ctags](http://ctags.sourceforge.net/) 选最下面那个 Source and binary for Windows 98/NT/2000/XP。

如果在用 **GNU/Linux**：
1. [toolchain for MIPS](http://codescape.mips.com/components/toolchain/2017.10-07/Codescape.GNU.Tools.Package.2017.10-07.for.MIPS.MTI.Bare.Metal.CentOS-5.x86_64.tar.gz)
2. 安装软件包。以 `dpkg` 包管理为例，运行
   `sudo apt install tree p7zip-full git ctags gcc g++ make libncurses5 libtinfo5 libnss3`

---
## Windows 设置
养成好习惯，将关于 NSCSCC 的东西放在一起。下面假设东西都放在 `D:\nscscc\`，当然你可以建立不同文件夹，但 **文件路径别太长** 。

### 解压两个包
1. 解压团队包
   假设解压内容放在 `D:\nscscc\pack_g\` ，然后将 `system_test_v0.01\windows_env` 移到 `D:\nscscc\soft\` 。

2. 解压 ctags
   去除根目录，解包到 `D:\nscscc\soft\ctags\` 。

完成后呈现效果如下。

![win_unpack](../../image/aa_env/win_unpack.png)

### 安装 Vivado
**我们这有现成的 Vivado 软件包**，“开课”前会集中帮助你们安装。若仍想自己折腾，参考 `D:\nscscc\pack_g\doc_v0.01\A06_vivado安装说明_v1.00.pdf` ，在此不作赘述。但注意下面几点：

1. 我们统一使用 Vivado 2019.2，须先在左侧点击“Vivado 存档”：
2.
![neu_vivado](../../image/aa_env/neu_vivado.png)

2. 更改 Vivado 安装路径，例如改到 `D:\Xilinx\` 。

### 设置环境变量
1. 按 Win + R 组合键，**运行** `systempropertiesadvanced.exe` （下文不再重复“运行”的方法）。

![win_env](../../image/aa_env/win_env.png)

2. 依次进入 高级 - 环境变量(<u>N</u>)... ，双击“系统变量”里的 `PATH`，点击“添加”，分别输入下列路径：
```powershell
D:\nscscc\soft\ctags
D:\nscscc\soft\qemu
D:\nscscc\soft\python37-32
D:\nscscc\soft\toolchain\bin
D:\Xilinx\Vivado\2019.2\bin
D:\Xilinx\Vivado\2019.2\tps\mingw\4.4.3\win64.o\nt64\bin
D:\Xilinx\Vivado\2019.2\tps\win64\git-2.16.2\bin
```

3. 重启电脑，验证设置。**运行** `cmd.exe` 。输入 `mips-mti-elf-gcc.exe -v` ，若输出版本信息，则恭喜你。

### 安装后优化
**运行** `powershell.exe`，把下列文本一股脑粘贴过去。
```powershell
mkdir "$env:APPDATA/Xilinx/Vivado"
cd "$env:APPDATA/Xilinx/Vivado"

$text=@'
set_param general.maxThreads 8
set_param synth.maxThreads 8
'@

echo "$text" > Vivado_init.tcl
exit
```

---
## GNU/Linux 设置
对 GNU/Linux，上手之后你甚至能脱离图形界面。不过仍是这句老话，咱们有条不紊地，慢慢来。下面仅提供简易提示；如果还是新手，不善折腾，就来找群里的大家吧！

刚开始我们已把绝大部分软件包装好了，几乎唯一的任务便是安装 Vivado 和线缆驱动、工具链。

### 解压团队包
仍须注意 **文件路径别太长** 。例如，将团队包解压到 `$HOME/nscscc/pack_g/` 。

### 安装 Vivado
**我们这有现成的 Vivado 软件包**，“开课”前会集中帮助你们安装。若仍想自己折腾，方法跟 Windows 的类似，但注意：
1. 可去 [Xilinx 下载中心](https://china.xilinx.com/support/download/index.html/content/xilinx/zh/downloadNav/vivado-design-tools/archive.html) 下载 Vivado 2019.2，但要注册 Xilinx 帐户。

2. 建议改安装路径到 `/opt/Xilinx/` 。推荐 **别用 root 身份** 运行 `xsetup` ，否则 Vivado 的快捷方式会出问题；那该怎么办？临时授予自己写权限。

3. 安装本体后 **以 root 身份** 运行下列命令，这样电脑就能和实验箱通信。
```bash
cd /opt/Xilinx/Vivado/2019.2/data/xicom/cable_drivers/lin64/install_script/install_drivers/
sudo ./install_drivers
```

### 安装 GNU 工具链
解压 .tar.gz 包，移到合适位置，添加环境变量，OK。

### 安装后优化
1. [改善 Java 字体渲染](https://forums.xilinx.com/t5/Design-Entry/Ugly-font-rendering-in-Vivado-under-Linux-Ubuntu-12-04/td-p/310851) ，不推荐直接修改 `/etc/environment` ，否则像 IntelliJ IDEA 这样的软件会报警告。考虑修改 Vivado 启动脚本 `/opt/Xilinx/Vivado/2019.2/bin/setupEnv.sh` ，在第 2 行后插入：
```bash
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=lcd -Dswing.aatext=true -Dsun.java2d.xrender=true"
```

2. 为 Vivado 添加环境变量，具体是 `/opt/Xilinx/Vivado/2019.2/bin` 这个目录。

---
## 设置 Visual Studio Code
萝卜青菜各有所爱，不同人群喜欢不同编辑器、不同扩展。下面仅以 VS Code 为例子，简单配置使之支持 Verilog 与 MIPS Assembly。

### 安装扩展
去 VS Code 自带应用商店装扩展。个人正在使用的扩展：
```bash
kdarkhan.mips
mshr-h.VerilogHDL
donjayamanne.githistory
```
### 配置 Verilog 扩展
点 VS Code 菜单的 文件 - 首选项 - 设置，点击界面上方的“用户”，然后搜索 `verilog.linting.linter` ，下拉选择 `xvlog` 即可。
