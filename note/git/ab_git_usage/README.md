# learn git in just a few seconds
[小说明书](https://gitee.com/help/articles/4107) [虚拟演练](https://oschina.gitee.io/learn-git-branching/)

上述内容由托管平台提供，帮助你学习 git。它是一个便于管理各版本代码的工具，例如出 bug 能轻松回退，各成员的代码既能互不干扰又能相互合并，等等。当然，**学 git 只是漫漫征途的极小一部分** ，它的机制比较多，日常使用中遇到问题再慢慢了解。

现在可图形化管理 git 的工具越来越多，例如功能全面的 IntelliJ IDEA、简洁流畅的 VS Code，甚至托管平台提供的 Web IDE。下面是 mxd 总结的常用命令。

## 简介
一个版本控制器

## 常用操作
下载源码：git clone 仓库地址

查看状态：git status

提交文件1：git add file

提交文件2：git commit -sm "some info"

完成提交： git push -u origin master

查看日志： git log

查看分支： git branch -a

切换分支或某个节点： git checkout branch-name

## 注意



