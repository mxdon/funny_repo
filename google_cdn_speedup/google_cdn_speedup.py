import requests
import time
import random
import urllib3
from bs4 import BeautifulSoup

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

heads = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}

website = "https://websites.ipaddress.com/"
des_domain = ["github.com","assets-cdn.github.com","github.global.ssl.fastly.net"]
des_ip = []

#des = open("/mnt/c/Windows/System32/drivers/etc/hosts","a")
des = open("/etc/hosts","a")
def getip(domain):
    url = website + domain
    try:	
        res = requests.get(url,timeout=30,headers=heads,verify=False)
        res.encoding = 'UTF-8'
        content = res.text
        #print(content)

        soup = BeautifulSoup(content,'html.parser')

        iplist = soup.find('ul',class_='comma-separated')
        iptmp = str(iplist.find("li").text) + "    " + domain
        print(iptmp)
 #       print("[\033[32m%s\033[0m](\033[34m%s\033[0m)" % (target,url) )
        #time.sleep(random.random()*3)
        #time.sleep(0.1)
    except:
        print("[\033[31m############   Something Error  #############\033[0m](\033[31m%s\033[0m)"% (url))
        iptmp = '' 

    des.write(iptmp + "\n")
#    return result

#def dealfile(filename):

#    src = open(filename,"r")
 #   lines = src.readlines()
  #  for line in lines:
   #     url = line.strip("\n").replace(" ","")
    #    result = gettitle(url)
     #   des.write('\n')
   # src.close()

if __name__ == '__main__':
    for idomain in des_domain:
        getip(idomain)
#            des.close()

# des.close()
