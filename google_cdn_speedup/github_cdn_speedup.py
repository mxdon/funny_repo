import requests
import time
import sys
import random
import urllib3
from bs4 import BeautifulSoup

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

heads = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}

# All the options
# full_domain = [github.githubassets.com, central.github.com, desktop.githubusercontent.com, camo.githubusercontent.com, github.map.fastly.net, github.global.ssl.fastly.net, gist.github.com, github.io, github.com, assets-cdn.github.com, api.github.com, raw.githubusercontent.com, user-images.githubusercontent.com, favicons.githubusercontent.com, avatars5.githubusercontent.com, avatars4.githubusercontent.com, avatars3.githubusercontent.com, avatars2.githubusercontent.com, avatars1.githubusercontent.com, avatars0.githubusercontent.com, avatars.githubusercontent.com, codeload.github.com, github-cloud.s3.amazonaws.com, github-com.s3.amazonaws.com, github-production-release-asset-2e65be.s3.amazonaws.com, github-production-user-asset-6210df.s3.amazonaws.com, github-production-repository-file-5c1aeb.s3.amazonaws.com, githubstatus.com, github.community, media.githubusercontent.com]

website = "https://websites.ipaddress.com/"
# Necessary options
des_domain = ["github.com","assets-cdn.github.com","github.global.ssl.fastly.net"]
des_ip = []

def getip(domain):
    url = website + domain
    try:	
        res = requests.get(url,timeout=30,headers=heads,verify=False)
        res.encoding = 'UTF-8'
        content = res.text
        #print(content)

        soup = BeautifulSoup(content,'html.parser')

        iplist = soup.find('ul',class_='comma-separated')
        iptmp = str(iplist.find("li").text) + "    " + domain
        print(iptmp)
    except:
        print("[\033[31m############   Something Error  #############\033[0m](\033[31m%s\033[0m)"% (url))
        iptmp = '' 

    des.write(iptmp + "\n")

if __name__ == '__main__':
    try:
        # Used in Windows
        # des = open("/mnt/c/Windows/System32/drivers/etc/hosts","a")
        # Used in Linux
        des = open("/etc/hosts","r")
        if "github" in str(des.readlines()):
            des.close()
            print("github dns configuration is already done!\n")
            sys.exit(0)

        des.close()
        des = open("/etc/hosts","a")
        print("# Add by mxdon, you can remove duplicate options manually\n")
        des.write("# Add by mxdon, you can remove duplicate options manually\n")
        for idomain in des_domain:
            getip(idomain)

    except:
        des.close()
