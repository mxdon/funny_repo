import requests
import time
import random
import urllib3
from bs4 import BeautifulSoup

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

heads = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}

des = open("res.txt","w")
def gettitle(url):
    try:	
        res = requests.get(url,timeout=30,headers=heads,verify=False)
        res.encoding = 'UTF-8'
        content = res.text
        #print(content)

        soup = BeautifulSoup(content,'html.parser')

        title = soup.find('title')
        h1 = soup.find('h1')
        target = title.text
        if len(target) == 0:
            target = h1.text
        
        result = "["+target+"]("+url+")\n"
        print("[\033[32m%s\033[0m](\033[34m%s\033[0m)" % (target,url) )
        #time.sleep(random.random()*3)
        #time.sleep(0.1)
    except:
        print("[\033[31m############   Something Error  #############\033[0m](\033[31m%s\033[0m)"% (url))
        result = '' 

    des.write(result)
    return result

def dealfile(filename):

    src = open(filename,"r")
    lines = src.readlines()
    for line in lines:
        url = line.strip("\n").replace(" ","")
        result = gettitle(url)
        des.write('\n')
    src.close()

if __name__ == '__main__':
    urlorfile = input("Please input a URL or a file name: \n")
    if urlorfile.startswith('http'):
        gettitle(urlorfile)
        des.close()
    else:
        dealfile(urlorfile)

    des.close()
